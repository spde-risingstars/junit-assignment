package com.innominds.junit;

public class Question4 {
	 
	public class DaemonThread extends Thread
	{
	    public DaemonThread(String name){
	        super(name);
	    }
	  
	    public void run()
	    {
	        
	        if(Thread.currentThread().isDaemon())
	        {
	            System.out.println(getName() + " is Daemon thread");
	        }
	          
	        else
	        {
	            System.out.println(getName() + " is User thread");
	        }
	    }
	      
	    
	}
	public boolean main()
    {
		Question4 question4=new Question4();
        DaemonThread t1 = question4.new DaemonThread("t1");
        DaemonThread t2 =question4.new DaemonThread("t2");
        DaemonThread t3 = question4.new DaemonThread("t3");
        t1.setDaemon(true);
        t1.start();
        t2.start();
        t3.start();
        return t2.isDaemon();
        // Setting user thread t1 to Daemon
        
    }
	private boolean isDaemon(DaemonThread t1) {
		return t1.isDaemon();
		
	}
}
