package com.innominds.junit;

public class que7 implements Comparable<que7> {

	private int id;
	private String name;
	private String department;

	public que7(int id, String name, String department) {
		this.id = id;
		this.name = name;
		this.department = department;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	@Override
	public int compareTo(que7 o) {

		if (o.getId() > this.getId()) {
			return 1;
		} else if (o.getId() < this.getId()) {
			return -1;
		}

		return 0;

	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", Department=" + department + "]";
	}
	public static void main(String[] args) {
		que7 emp1=new que7(1,"sri lakshmi","Spde");
		System.out.println(emp1.toString());
	}

}