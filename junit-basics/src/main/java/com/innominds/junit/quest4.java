package com.innominds.junit;

import java.time.LocalDateTime;
import java.util.Timer;

import javax.sound.sampled.Line.Info;

public class quest4<readonly> {
	public interface IAddTimeSpanToCurrentDateAndTime
	{
	    LocalDateTime Execute(Timer input);
	}

	public class AddTimeSpanToCurrentDateAndTime
	{
	    private readonly INow _now;

	    public AddTimeSpanToCurrentDateAndTime(Info now)
	    {
	        this.INow = now;
	    }

	    public DateTime Execute(TimeSpan input)
	    {
	        var currentDateAndTime = this.INow.Execute();

	        return currentDateAndTime.Add(input);
	    }
	}

}
