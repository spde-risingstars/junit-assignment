package com.innominds.junit;

import java.util.ArrayList;
import java.util.List;

public class ques4 {
	private List<Inner> inners = new ArrayList<Inner>();

	public List<Inner> getInners() {
		return inners;
	}

	public void addInner(Inner i) {
		inners.add(i);
	}

	public void removeInner(Inner i) {
		inners.remove(i);
	}

	public class Inner {
		private int equalityField;
		private int otherFields;

		public int getEqualityField() {
			return equalityField;
		}

		public Inner(int field) {
			this.equalityField = field;
		}

		@Override
		public boolean equals(Object o) {
			if (o == null)
				return false;
			if (o.getClass() != this.getClass())
				return false;
			Inner other = (Inner) o;
			if (equalityField == other.getEqualityField())
				return true;
			return false;
		}
	}
}