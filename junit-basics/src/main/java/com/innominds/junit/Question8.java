package com.innominds.junit;

import java.util.stream.IntStream;

public class Question8 {
	 public void readStreamTest()
	    {
	        IntStream target = new ReadStream(); 
	        Stream stream = new MemoryStream(); 
	        StreamWriter sw = new StreamWriter(stream);

	        string line = "My Line 1\n¬My Line 2\nMy Line 3\nMy Line 4\nMy Line 5\nMy Line 6\nMy Line 7";
	        int numLines = 5; // TODO: Initialize to an appropriate value
	        List<FileLine> lines = new List<FileLine>();
	        int i = 0;
	        while ( i != 7 )
	        {
	            lines.Add(new FileLine()
	            {
	                lineNo = i,
	                lineContent = line
	            });
	            i++;
	        }
	        List<FileLine> expected = lines; // TODO: Initialize to an appropriate value
	        List<FileLine> actual;
	        actual = target.readStream(stream, numLines);
	        Assert.IsNotNull(expected);
	    }

}
