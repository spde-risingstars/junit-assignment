package com.innominds.junit;

import java.util.Arrays;

public class Question10 {
	
	public static int countofDuplicates(String input) {
		
			char[] arr=input.toCharArray();
			Arrays.sort(arr);
			 int i=0;
			 int j=i+1;
			 int count=0;
			while(j<arr.length) {
				if(arr[i]==arr[j]) {
					count++;
				}
				i++;
				j++;
			}
		
		return count;
	}
}



