package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.text.html.parser.Entity;

import org.junit.jupiter.api.Test;

class Question7Test {

	@Test
	void test() {
		class SystemTime
		{
		    public static Func<DateTime> Now = () => DateTime.Now;
		}
		Entity.LastChange = SystemTime.Now();

		SystemTime.Now = () => new DateTime(2000,1,1);
		repository.ResetFailures(failedMsgs); 
		SystemTime.Now = () => new DateTime(2000,1,2);
		var msgs = repository.GetAllReadyMessages(); 
		Assert.AreEqual(2, msgs.Length);
	}

}
