package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class ques9Test {

	@Test
	void testSquares() {
		ques9 qu = new ques9();
		List<Integer> input = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		List<Integer> expected = Arrays.asList(1, 4, 9, 16, 25, 36, 49, 64, 81, 100);
		assertArrayEquals(expected.toArray(), qu.squares(input).toArray());

	}

}
