package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.innominds.junit.question14.EmployeeDetails;

class question14Test {

	@Test
	<EmpBusinessLogic> void test() {
		class TestEmployeeDetails {
		   EmpBusinessLogic empBusinessLogic = new EmpBusinessLogic();
		   EmployeeDetails employee = new EmployeeDetails();

		   @Test
		   public void testCalculateAppriasal() {
		      employee.setName("Rajeev");
		      employee.setAge(25);
		      employee.setMonthlySalary(8000);
				
		      double appraisal = empBusinessLogic.calculateAppraisal(employee);
		      assertEquals(500, appraisal, 0.0);
		   }

		   @Test
		   public void testCalculateYearlySalary() {
		      employee.setName("Rajeev");
		      employee.setAge(25);
		      employee.setMonthlySalary(8000);
				
		      double salary = empBusinessLogic.calculateYearlySalary(employee);
		      assertEquals(96000, salary, 0.0);
		   }
		}
	}

}
