package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class lambdaTest {


	
		@Test
	    @DisplayName("Addition using method")
	    void testAdd() {
	        assertEquals(14,lambda.add(8, 6));
	    }
	    @Test
	    @DisplayName("Addition using Lamda expression")
	    void testAdditionUsingLambda() {
	        assertEquals(10, lambda.additionOp.operation(1, 9));
	    }
}
