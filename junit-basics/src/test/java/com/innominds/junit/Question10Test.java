package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question10Test {

	@Test
	void testforOneduplicate() {
		String input="hello";
		int result=Question10.countofDuplicates(input);
		assertNotNull(input);
		assertEquals(1,result);
	}
	
	@Test
	void testforTwoduplicate() {
		String input="helllo";
		int result=Question10.countofDuplicates(input);
		assertNotNull(input);
		assertEquals(2,result);
	}
	
	@Test
	void testforThree() {
		String input="hellloo";
		int result=Question10.countofDuplicates(input);
		assertNotNull(input);
		assertEquals(3,result);
	}
	
	@Test
	void testforEmptyString() {
		String input="helloo";
		int result=Question10.countofDuplicates(input);
		assertNotNull(input);
		assertEquals(2,result);
	}


}
