package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class personTest {

	@Test
	void testDigit() {
		assertEquals("Digit", Person.checkCharacter('2'));
	}
 
	@Test
	void testUpper1() {

		char ch = 'A';
		assertEquals("Uppercase", Person.checkCharacter(ch));
	}

	@Test
	void testUpper2() {
		char ch = 'b';
		assertNotEquals("Uppercase", Person.checkCharacter(ch));

	}

	@Test
	void testLower() {

		char ch = 'a';
		assertEquals("Lowercase", Person.checkCharacter(ch));
	}
}
