package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

public class ques4Test {
	@Test
	public void testRemoveInner() {
	ques4.Inner[] mockInner = new ques4.Inner[2];
		mockInner[0] = EasyMock.createMock(ques4.Inner.class);
		mockInner[1] = EasyMock.createMock(ques4.Inner.class);

		ques4 e = new ques4();
		e.addInner(mockInner[0]);
		e.addInner(mockInner[1]);
		e.removeInner(mockInner[0]);
		e.removeInner(mockInner[0]);
		assertEquals(0, e.getInners().size());
	}
}
