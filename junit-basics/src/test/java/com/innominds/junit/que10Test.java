package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

class que10Test {

	@Test
	void test() {
		package compress;
		class CompressionTest {
		 
		    String testStr = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
		    @Test   
		    void compressByte() throws IOException {
		        byte[] input = testStr.getBytes();     
		        byte[] op = CompressionUtil.compress(input); 
		        System.out.println("original data length " + input.length + ",  compressed data length " + op.length);
		        byte[] org = CompressionUtil.decompress(op);     
		        System.out.println(org.length);   
		        System.out.println(new String(org, StandardCharsets.UTF_8));    }
		 
		    @Test   
		    void compress() throws IOException {
		 
		        String op = CompressionUtil.compressAndReturnB64(testStr);   
		        System.out.println("Compressed data b64" + op);
		        String org = CompressionUtil.decompressB64(op);    
		        System.out.println("Original text" + org);
		    }
		 
		}
	}

}
